const electron = require('electron');

// controls app life
const {app,BrowserWindow,Menu} = electron;

const path = require('path');
const url = require('url');
const ISAMAC = process.platform === 'darwin';

// Keep a global reference of the window object, if you don't the window will be closed
// automaticall when javascript object is garbage collected

let mainWindow;
let launchPane;

let workerId = process.env.ELECTRON_WORKER_ID; // Worker id is useful for logging

let createWindow = ()=>{
    // Creates the browser window
    mainWindow = new BrowserWindow({
        width: 2560,
        height: 1440
    });

    // Load the index.html of the app
    mainWindow.loadURL('http://localhost:3000');


    // Build menu from template
    const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);

    // Insert Menu
    Menu.setApplicationMenu(mainMenu);

    mainWindow.toggleDevTools();

    // listens to the `message` event in the process object
    process.on('message', (data)=>{
        if(!data){
            return;
        }

        // `electron-workers` will try to verify if your worker is alive sending you a `ping` event
        if(data.workerEvent=== 'ping'){
            // responding the ping call.. this will notify `electron-workers` that your process is alive
            process.send({workerEvent: 'pong'});
        }else if(data.workerEvent === 'task'){
            // Whenever a new task is executed you will receive a 'task' event

            console.log(data); //data -> { workerEvent: 'task', taskId: '....', payload: <whatever you have passed to `.execute`> }

            console.log(data.payload.someData); // -> someData


            // you can do whatever you want here..

            // when the task has been processed,
            // respond with a `taskResponse` event, the `taskId` that you have received, and a custom `response`.
            // You can specify an `error` field if you want to indicate that something went wrong
            process.send({
                workerEvent: 'taskResponse',
                taskId: data.taskId,
                response: {
                    value: data.payload.someData
                }
            });
        }

    })

    // Emitted when the window is closed
    mainWindow.on('closed', ()=>{
        // dereference the window object
        mainWindow = null;
    })
}



const mainMenuTemplate = [
    {
      label: 'Electron',
      submenu: [
          {
              label: "About Application",
              selector: "orderFrontStandardAboutPanel:"
          },
          {
              label: "Quit",
              accelerator: "Command+Q",
              click: function(){
                  app.quit();
              }
          }
      ]
    },
    {
        label: 'Settings'
    },
    {
      label: "Edit",
      submenu: [
          { label: "Undo", accelerator: "CmdOrCtrl+Z", selector: "undo:" },
          { label: "Redo", accelerator: "Shift+CmdOrCtrl+Z", selector: "redo:" },
          { type: "separator" },
          { label: "Cut", accelerator: "CmdOrCtrl+X", selector: "cut:" },
          { label: "Copy", accelerator: "CmdOrCtrl+C", selector: "copy:" },
          { label: "Paste", accelerator: "CmdOrCtrl+V", selector: "paste:" },
          { label: "Select All", accelerator: "CmdOrCtrl+A", selector: "selectAll:" }
      ]
    },
    {
        label: 'Reload',
        role: 'forcereload'
    },
]

// This method will be called when electron has finished initalization and is reqdy to create browsers windows
app.on('ready',createWindow);

// Quit when all windows are closed
app.on('window-all-closed', ()=>{
    // OSX keeps all apps and their menu bar active until the user quits explcitly
    if(process.platform !== 'darwin'){
        app.quit();
    }
})

app.on('activate', ()=>{
    // OSX its common to recreate a window in the app when the dock icon is clicked and no other window is opened
    if(mainWindow === null){
        createWindow()
    }
})

// If this is a mac ensure there is a gap for the first item in the main menu template
if(ISAMAC){
    // mainMenuTemplate.unshift({});
}