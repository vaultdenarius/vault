import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';


import Landing from './mainGui/main/Landing';
import Vault from './mainGui/main/views/Vault';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<Router>
    <Switch>
        <Route exact path="/" component={Landing}/>
        <Route path="/vault" component={Vault}/>
    </Switch>
</Router>, document.getElementById('App'));
registerServiceWorker();
