import React, {Component} from 'react';
import update from 'immutability-helper';
import {Form, FormGroup, FormControl, ControlLabel, Col, Button, Modal,Radio} from 'react-bootstrap';
import CONSTANTS from '../../../../const/index';



const TickerChoice = ({toggleSwitch, isShowingBinance}) => {
    return (
        <div className="switchContainer">
            <p>Binance</p>
            <label className="switch">
                <input type="checkbox" onClick={toggleSwitch} value={isShowingBinance}/><span
                className="slider round"></span>
            </label>
            <p>Bittrex</p>
        </div>
    )
}


/**
 * TODO add Valuations
 */
class AddCoin extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showingBinanceTickers: true,
            coinSelectChoice: '',
            userInput:{
                exchange: 'binance', // The Exchange where the ticker was purchased
                pricePerCoin: 0, // price per ticker
                fee: 0, // transaction fee
                feeCoin: 'BTC', // ticker used to pay fee
                totalPrice: 0, // total cost of transaction
                ticker: props.binanceSymbolAndPrice[0].symbol, // Gets the first ticker in binance
                name: '', // TODO fill in the name for the coin here
                coinUsedForPurchase: 'BTC', // Coin thats used to make the purchase of the current ticker
                amount: 0, // amount of ticker
                date: new Date(), // transaction date
                type: 'BUY', // type of exchange
                method: 'userAdd' // method of input into the system
            }

        }
        this.binding();
    }

    binding() {
        this.toggleSwitch = this.toggleSwitch.bind(this);
        this.saveUserInput = this.saveUserInput.bind(this);
        this.toggleBuyAndSell = this.toggleBuyAndSell.bind(this);
        this.updateState = this.updateState.bind(this);
    }

    /**
     * The logic is weird but the gist is
     * If the switch is not checked then that means binance is selected
     * If the switch is selected then bittrex is selected
     * @param e
     */
    toggleSwitch(e) {
        this.setState({
            userInput: update(this.state.userInput,{
                $merge: {
                    showingBinanceTickers: !e.target.checked,
                    exchange: e.target.checked? 'binance' : 'bittrex'
                }
            })
        })
    }

    /**
     * returning list of all the ticker symbols for all the coins
     * @returns {any[]}
     */
    displayListOfAllCoins(){
        return Object.keys(CONSTANTS.COINANDTICKER).map((ticker,index)=>{
            return (
                <option key={index} value={ticker}>{ticker}</option>
            )
        })
    }

    /**
     * Based on what exchange info is being added
     * toggle between showing the ticker symbols in those exchanges
     */
    displaySelectOptions(){
        const {binanceSymbolAndPrice,bittrexSymbolAndPrice} = this.props;
        const {showingBinanceTickers} = this.state;

        if(showingBinanceTickers){
            return binanceSymbolAndPrice.map((binance,index) =>{
                const coinProp = CONSTANTS.splitCoinIntoTickerAndPurchaseCoin(binance.symbol);
                return (
                    <option key={index} value={coinProp.ticker} >{coinProp.ticker}</option>
                )
            })
        }else{
            return bittrexSymbolAndPrice.map((bittrex,index) =>{
                return (
                    <option key={index} value={bittrex.Currency}>{bittrex.Currency}</option>
                )
            })
        }
    }

    /**
     * @param type
     */
    toggleBuyAndSell(type){
        this.setState({
            userInput: update(this.state.userInput,{
                $merge: {
                    type: type === 'BUY'? 'SELL': 'BUY'
                }
            })
        })
    }

    saveUserInput(){
        const {updateUserCoins,closeModal} = this.props;

        updateUserCoins({...this.state.userInput});

        closeModal();
    }

    updateState(key,value){
        this.setState({
            userInput: update(this.state.userInput,{
                $merge: {
                    [key]: value
                }
            })
        })
    }


    render() {
        const {showModal, closeModal} = this.props;
        const {showingBinanceTickers,userInput : {pricePerCoin,totalPrice,ticker,fee,feeCoin,amount,date}} = this.state;
        return (
            <Modal show={showModal} onHide={closeModal}>
                <Modal.Header>
                    <Modal.Title>Add Coin</Modal.Title>
                    <TickerChoice toggleSwitch={this.toggleSwitch} showingBinanceTickers={showingBinanceTickers}/>
                </Modal.Header>

                <Modal.Body>
                    <Form horizontal>
                        <FormGroup controlId="tickerSelector">
                            <Col sm={4}>
                                <ControlLabel>Coin</ControlLabel>
                                <FormControl componentClass="select" value={ticker} onChange={(e)=> this.updateState('ticker',e.target.value)}>
                                    {this.displaySelectOptions()}
                                </FormControl>
                            </Col>
                            <Col sm={4}>
                                <ControlLabel>Transaction Date</ControlLabel>
                                <FormControl type="date" value={date} onChange={(e) => this.updateState('date',e.target.value)}/>
                            </Col>
                            <Col sm={4}>
                                <ControlLabel>Amount</ControlLabel>
                                <FormControl type="number" value={amount} onChange={(e) => this.updateState('amount',e.target.value)}/>
                            </Col>
                        </FormGroup>
                        <FormGroup controlId={"priceInput"}>
                            <Col sm={4}>
                                <ControlLabel>Price Per Coin</ControlLabel>
                                <FormControl type="number" value={pricePerCoin} onChange={(e)=> this.updateState('pricePerCoin',e.target.value)}/>
                            </Col>
                            <Col sm={4}>
                                <ControlLabel>Total Price</ControlLabel>
                                <FormControl type="number" value={totalPrice} onChange={(e)=> this.updateState('totalPrice',e.target.value)}/>
                            </Col>
                            <Col sm={2}>
                                <ControlLabel>Buy</ControlLabel>
                                <Radio name="radioGroup" onClick={this.toggleBuyAndSell}></Radio>
                            </Col>
                            <Col sm={2}>
                                <ControlLabel>Sell</ControlLabel>
                                <Radio name="radioGroup" onClick={this.toggleBuyAndSell}></Radio>
                            </Col>
                        </FormGroup>
                        <FormGroup>
                            <Col sm={6}>
                                <ControlLabel>Fee Coin</ControlLabel>
                                <FormControl componentClass="select" value={feeCoin} onChange={(e)=> this.updateState('feeCoin', e.target.value)}>
                                    {this.displayListOfAllCoins()}
                                </FormControl>
                            </Col>
                            <Col sm={6}>
                                <ControlLabel>Fee</ControlLabel>
                                <FormControl type="number" value={fee} onChange={(e)=> this.updateState('fee',e.target.value)}/>
                            </Col>
                        </FormGroup>
                    </Form>
                </Modal.Body>

                <Modal.Footer>
                    <Button onClick={closeModal}>Close</Button>
                    <Button bsStyle="primary" onClick={this.saveUserInput}>Save changes</Button>
                </Modal.Footer>
            </Modal>
        )
    }
}

export default AddCoin;