import React, {Component} from 'react';
import {Button, Col, Form, FormControl, FormGroup, Modal, Row} from 'react-bootstrap';
import Dropzone from 'react-dropzone'
import './styles/uploadTradeHistory.css';
import CONSTANT from '../../../../const/index';

// This is needed so that fs can be loaded up properly
const fs = window.require('fs');
const csv = window.require('csvtojson');





class UploadTradeHistory extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isBinanceCsv: true,
            filePath: '',
            data: []

        }
        this.binding();
    }

    binding() {
        this.handleFileUpload = this.handleFileUpload.bind(this);
        this.processData = this.processData.bind(this);
    }

    /**
     * Based on the type of file upload
     * gets the csv data in file
     * */
    handleFileUpload(type, file) {
        const {isBinanceCsv} = this.state;

        let filePath = "";

        if (type === 'drop') {
            filePath = file[0].path;
        } else if (type === 'input') {
            filePath = file.target.files[0].path;
        }

        if (isBinanceCsv) {
            /*
                The purpose of this is to update the header of the binance csv
                the title is usually caps and they become my object keys so i just make them all lowercase here
            */
            fs.readFile(filePath, 'utf-8', (err, data) => {
                if (err) {
                    console.log(err);
                    return;
                }
                let firstLineBreak = data.indexOf('\n')
                let header = data.substring(0, firstLineBreak)
                header = header.split(',').map((str) => {
                    if (str === "Fee Coin" || "fee ticker") {
                        return str.split(" ").join("").toLowerCase();
                    }
                    return str.toLowerCase();
                }).join(',');

                let newData = header + data.slice(firstLineBreak, data.length);

                fs.writeFile(filePath, newData, (err) => {
                    if (err) {
                        console.log(err);
                        return;
                    }

                    csv().fromFile(filePath).on('end_parsed', (obj) => {
                        this.setState({
                            filePath,
                            data: obj
                        })
                    })
                })
            })
        }

    }

    /**
     * Transforms the json objects into an object where each key
     * is an array of the trading operations
     * */
    processData() {
        const {data, isBinanceCsv} = this.state;
        const {bundleImportedData, closeModal} = this.props;

        let transformedCsv = {}



        if (isBinanceCsv) {
            data.forEach((coin) => {
                const coinProps = CONSTANT.splitCoinIntoTickerAndPurchaseCoin(coin.market);

                let tempObject = {
                    exchange: 'binance', // The Exchange where the ticker was purchased
                    pricePerCoin: coin.price, // price per ticker
                    fee: coin.fee, // transaction fee
                    feeCoin: coin.feecoin, // ticker used to pay fee
                    totalPrice: coin.total, // total cost of transaction
                    coin: coinProps.ticker, // Gets the ticker symbol
                    coinUsedForPurchase: coinProps.coinUsedForPurchase, // Coin thats used to make the purchase of the current ticker
                    amount: coin.amount, // amount of ticker
                    date: new Date(coin.date), // transaction date
                    type: coin.type, // type of exchange
                    method: 'userUpload' // method of input into the system
                }

                if (transformedCsv.hasOwnProperty(coinProps.ticker)) {
                    transformedCsv[coinProps.ticker].push(tempObject);
                } else {
                    transformedCsv[coinProps.ticker] = [];
                    transformedCsv[coinProps.ticker].push(tempObject);
                }
            })
        }
        bundleImportedData(transformedCsv, isBinanceCsv);
        closeModal();
    }

    render() {
        const {showModal, closeModal} = this.props;
        return (
            <Modal show={showModal} onHide={closeModal}>
                <Modal.Header>
                    <Modal.Title>Import TradeHistory</Modal.Title>
                    {/*<TickerChoice toggleSwitch={this.toggleSwitch} showingBinanceTickers={showingBinanceTickers}/>*/}
                </Modal.Header>

                <Modal.Body>
                    <Form>
                        <FormGroup controlId="manualFileLoad">
                            <Col sm={2}>
                                Load File
                            </Col>
                            <Col sm={10}>
                                <FormControl type="file" onChange={this.handleFileUpload.bind(this, 'input')}/>
                            </Col>
                        </FormGroup>
                        <FormGroup controlId="dragAndDropFile">
                            <Row>
                                <Col sm={12}>
                                    Drag & Drop File
                                </Col>
                            </Row>
                            <Row>
                                <Col sm={12}>
                                    <Dropzone
                                        accept={'text/csv'}
                                        onDrop={this.handleFileUpload.bind(this, 'drop')}>
                                        <p>Try dropping some files here, or click to select files to upload.</p>
                                    </Dropzone>
                                </Col>
                            </Row>
                        </FormGroup>
                    </Form>
                </Modal.Body>

                <Modal.Footer>
                    <Button onClick={closeModal}>Close</Button>
                    <Button bsStyle="primary" onClick={this.processData}>Process Data</Button>
                </Modal.Footer>
            </Modal>
        )
    }
}

export default UploadTradeHistory;