import React, {Component} from 'react';
import {Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

import AddCoin from './coinMenuItems/addCoin';
import UploadTradeHistory from './coinMenuItems/uploadTradeHistory';


/**
 * Main menu for navigating inside of app
 */
class CoinMenu extends Component{
    constructor(props){
        super(props);

        this.state = {
            addCoin : {
                name: 'addCoin',
                show: false
            },
            uploadTradeHistory: {
                name: 'uploadTradeHistory',
                show: false
            }
        }
        this.binding();
    }

    /**
     * Sets up bindings to keep 'this' referencing to base class
     */
    binding(){
        this.closeModal = this.closeModal.bind(this);
    }

    /**
     * Closes out whatever modal is passed
     * @param menuItem
     */
    closeModal(menuItem) {
        this.setState({
            [menuItem.name]: {
                name: menuItem.name,
                show: false
            }
        })
    }

    /**
     * Opens whatever modal is passed
     * @param menuItem
     * @constructor
     */
    LaunchMenuItem(menuItem){
        this.setState({
            [menuItem.name]: {
                name: menuItem.name,
                show: true
            }
        })
    }


    render(){
        const {binanceSymbolAndPrice,bittrexSymbolAndPrice,btcToUsd,updateUserCoins,bundleImportedData} = this.props;
        const {addCoin,uploadTradeHistory} = this.state;
        return(
            <ul>
                <Button><Link to={'/'}>Home</Link></Button>
                <Button onClick={this.LaunchMenuItem.bind(this,addCoin)}>Add Coin</Button>
                <Button onClick={this.LaunchMenuItem.bind(this,uploadTradeHistory)}>Upload Trade History</Button>
                <Button >logout</Button>
                { addCoin.show ? <AddCoin
                    showModal={true}
                    btcToUsd={btcToUsd}
                    updateUserCoins={updateUserCoins}
                    closeModal={this.closeModal.bind(this,addCoin)}
                    bittrexSymbolAndPrice={bittrexSymbolAndPrice}
                    binanceSymbolAndPrice={binanceSymbolAndPrice}
                /> : null
                }
                {
                    uploadTradeHistory.show ? <UploadTradeHistory
                        showModal={true}
                        bundleImportedData={bundleImportedData}
                        closeModal={this.closeModal.bind(this,uploadTradeHistory)}
                    /> : null
                }
            </ul>
        )
    }
}

export default CoinMenu