import React from 'react';
import './styles/switch.css';

/**
 * TODO Contemplate updating the USD and BTC symbol color on switch
 * Switch which changes whether the currency being displayed is usd or btc throughout the app
 * @param toggleShowing
 * @param isShowingUSCurrency
 * @returns {*}
 * @constructor
 */
const CurrencySwitch = ({toggleShowing, isShowingUSCurrency}) => {
    return (
        <div>
            <div className="switchContainer">
                <p>USD</p>
                <label className="switch">
                    <input type="checkbox" onClick={toggleShowing} value={isShowingUSCurrency}/><span
                    className="slider round"></span>
                </label>
                <p>BTC</p>
            </div>
        </div>
    )
}

export default CurrencySwitch