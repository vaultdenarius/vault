import React from 'react';
import {ListGroup, ListGroupItem} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import './styles/coinList.css'


const CoinList = ({exchanges, match}) => {


    let transformExchangestoList = () => {
        return exchanges.reduce((accum, current) => {
            if (accum.length == 0) {
                return accum.concat(current)
            }

            let coinObj = {}

            Object.keys(accum).forEach((key) => {
                coinObj[key] = accum[key]
            })

            Object.keys(current).forEach((key) => {
                if (key in coinObj) {
                    coinObj[key].concat(current[key])
                } else {
                    coinObj[key] = current[key]
                }
            })
            return coinObj
        }, [])
    }


    return (
        <ListGroup id="coinlist">
            {
                transformExchangestoList().map((coin, index) => {
                    return (
                        <ListGroupItem key={index}>
                            <Link to={{
                                pathname: `${match.path}/${coin}`,
                                state: {
                                    coinInfo: exchanges.binance[coin]
                                }
                            }}>{coin}</Link>
                        </ListGroupItem>
                    )
                })
            }
        </ListGroup>

    )
}

export default CoinList;