import React, {Component} from 'react';
import {Table} from 'react-bootstrap';

import TableRow from './components/tableRow';

import CONSTANT from '../../../../const/index';
import './styles/coininfo.css'
import './styles/coinpage1.css';




//TODO add in all the css files and images to show the icons for each ticker
class CoinInfo extends Component{
    constructor(props){
        super(props);

        this.state = {
            totalValue: 0, // shows the total value of the investment
            totalProfit: 0, // shows total gained from initial investment
            initalInvestment: 0, // shows the initial investment 
            totalAmount: 0, // total amount held from the exchange action
        }
    }

    binding(){

    }

    componentDidMount(){
        const {location: {state: {coinInfo}}} = this.props;

        // this.setState({
        //     totalAmount: this.determineTotalAmount(coinInfo),
        // });
    }

    /**
     * TODO Potentially expensive operation find a better way to do this
     *
     * Currently checks using binance procedures to look for
     * */
    determineTotalAmount(coinInfo){
        // Determines the total amount of the ticker based on buy and sell operations inside of the ticker list
        let currentCoinAmount = coinInfo.reduce((accum,coin)=>{
            if(coin.type.toLowerCase() === 'buy'){
                return accum + coin.amount;
            }else{
                return accum - coin.amount;
            }
        },0);
    }



    // Searches the tag list to be able to get the sprite img display for the ticker
    getCoinSprite(coin){
        return CONSTANT.TAGLIST.filter((cssTag) => {
            let tagStr = cssTag.split("_")[2];
            return coin.indexOf(tagStr) > -1;
        })[0];
    }

    // Searches the list of coins for the ticker name
    getCoinName(coin){
        return CONSTANT.COINANDTICKER.filter((item) => {
            var lowerTicker = item.ticker.toLowerCase();
            // Checks if the ticker contains btc or eth in the last part
            return coin.indexOf(lowerTicker) > -1 && coin.indexOf(lowerTicker) === 0;
        })[0].name;
    }

    render(){
        const {location :{state:{coinInfo}},translateCurrency} = this.props;
        const coin = coinInfo[0].ticker;
        return(
            <div id="coinInfo">

                <div className="header">
                    <div className="coinIcon">
                        {/*TODO create a scraper that will scrape all the coins icons imgs for me*/}
                        <div className={this.getCoinSprite(coin.toLowerCase())}></div>
                    </div>
                    <div className="cointitle">
                        <h1>{CONSTANT.COINANDTICKER[coin]}</h1>
                    </div>
                </div>
                <div className="contentArea">
                    <div className="factbar">
                        <div className="barItem">Amount Owned</div>
                        <div className="barItem">Current Price</div>
                        <div className="barItem">Total Value</div>
                        <div className="barItem">Total Profit</div>
                    </div>

                    <div className="bottomHalf">
                        <div className="buySellTable">
                            <div className="filters"></div>
                            {/* TODO Freeze pane for the top column headers */}
                            <Table striped bordered condensed hover>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Exchange</th>
                                        <th>Date</th>
                                        <th>Type</th>
                                        <th>Amount</th>
                                        <th>Price Per Coin</th>
                                        <th>Fee</th>
                                        <th>Total Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {
                                    coinInfo.map((coinInfo,index)=>{
                                        return (
                                            <TableRow key={index} coinInfo={coinInfo} index={index} translateCurrency={translateCurrency}/>
                                        )
                                    })
                                }
                                </tbody>
                            </Table>
                        </div>
                        <div className="unknown">
                            {/*TODO make a chart based on all of the data */}
                            {/*Chart area */}
                            {/*{JSON.stringify(this.props)}*/}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default CoinInfo;