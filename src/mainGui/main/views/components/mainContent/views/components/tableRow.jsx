import React from 'react';
import moment from 'moment';

const TableRow = ({coinInfo,index,translateCurrency}) => {
    return(
        <tr>
            <td>{index +1}</td>
            <td>{coinInfo.exchange}</td>
            <td>{moment(coinInfo.date).format('M/D/Y h:m a')}</td>
            <td>{coinInfo.type}</td>
            <td>{coinInfo.amount}</td>
            <td>{translateCurrency(coinInfo.pricePerCoin)}</td>
            <td>{coinInfo.fee}</td>
            <td>{translateCurrency(coinInfo.totalPrice)}</td>
        </tr>
    )
}
export default TableRow;