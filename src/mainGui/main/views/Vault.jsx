import React, {Component} from 'react';
import axios from 'axios';
import update from 'immutability-helper';
import {Form,Button,FormControl,FormGroup,Checkbox,Col} from 'react-bootstrap'
import {Route,Switch,Redirect} from 'react-router-dom';
import { CircleLoader } from 'react-spinners';
import cryptocompare from 'cryptocompare';


import CurrencySwitch from './components/currencySwitch';
import CoinList from './components/coinList';
import CoinMenu from './components/coinMenu/coinMenu';
import Home from './components/mainContent/home';
import CoinInfo from './components/mainContent/views/coinInfo';

// const electron = window.require('electron');
// const {ipcRenderer} = electron;

// Allows communication between different windows
// console.log(ipcRenderer);

class Vault extends Component {
    constructor(props) {
        super(props);


        this.state = {
            loading: false,
            isShowingBTCCurrency: false,
            isFiltered: false,
            filterSettings:{

            },
            isBinanceApiActive: false,
            isBittrexApiActive: false,
            binanceSymbolAndPrice: [],
            bittrexSymbolAndPrice: [],
            btcBlock: {},
            calculatedBtcValue: 0,
            user: {
                exchanges: [],
                binance: {},
                bittrex: {},
                filteredList: {}
            }
        }
        this.binding();
    }

    // Sets up bindings which allow functions to retain their reference to 'this' from the source
    binding() {
        this.toggleShowing = this.toggleShowing.bind(this);
        this.btcToUsd = this.btcToUsd.bind(this);
        this.updateUserCoins = this.updateUserCoins.bind(this);
        this.bundleImportedData = this.bundleImportedData.bind(this);
        this.toggleLoading = this.toggleLoading.bind(this);
        this.translateCurrency = this.translateCurrency.bind(this);
    }

    toggleLoading(bool,launchHistory){
        this.setState({
            loading: bool
        },()=>{
            setTimeout(launchHistory(),1000)
            ;
        })
    }

    /**
     * Updates the list of the Users Coin
     * @param coin
     */
    updateUserCoins(coin) {
        const exchange = coin.exchange;
        // Gets a true copy of all of the data at the exchange
        let currentExchangeVal = update(this.state.user[exchange],{
            $merge: {
            }
        });

        // Checks if the exchange contains the ticker
        // If not create a key init with an array of the ticker and update the state
        // If it does just pushes in the new object into the existing array
        if(!currentExchangeVal.hasOwnProperty(coin.ticker)){
            currentExchangeVal[coin.ticker] = [coin];
            this.setState({
                user: update(this.state.user, {
                    [exchange]: {
                        $set: currentExchangeVal
                    }
                })
            })
        }else{
            this.setState({
                user: update(this.state.user, {
                    [exchange]: {
                        [coin.ticker]: {
                            $push : [coin]
                        }
                    }
                })
            })
        }

    }

    componentDidMount() {
        // TODO
        // Tests the connection to see if binance api is active
        // axios.get('/api/v1/ping')


        // Once the components has launched grab all of the symbol prices from binance
        // Also set the value for BTC
        axios.get('/api/v3/ticker/price').then((res) => {
            // Update the state with the data
            this.setState({
                isBinanceApiActive: true,
                binanceSymbolAndPrice: res.data,
                btcBlock: res.data.filter(i => i.symbol === "BTCUSDT")[0]
            })
        }).then(err => console.error(err));

        axios.get('/api/v1.1/public/getcurrencies').then((res) => {
            this.setState({
                isBittrexApiActive: true,
                bittrexSymbolAndPrice: res.data.result
            })
        }).then(err => console.error(err));


        cryptocompare.coinList().then((coinlist) =>{
            console.log(coinlist)
        })

        cryptocompare.priceHistorical('ETC',['USD','BTC'], new Date('2017-11-11')).then( prices => {
            console.log(prices);

        })
    }

    /**
     * Toggles the displaying of all values in btc or usd
     * @param e
     */
    toggleShowing(e) {
        this.setState({
            isShowingBTCCurrency: e.target.checked
        })
    }

    /**
     * Bundles all of the imported data into the app
     * @param importedData
     * @param isBinance
     */
    bundleImportedData(importedData,isBinance){
        const exchange = isBinance? 'binance' : 'bittrex';
        this.setState({
            user: update( this.state.user, {
                    [exchange] : {
                        $merge: importedData
                    }
                }
            )
        })
    }

    /**
     * Checks if any user coins have added in the exchanges
     * @returns {boolean}
     */
    isUserCoinEmpty(){
        const {user:{exchanges}} = this.state;

        return exchanges.filter((exchange) =>{
            return Object.keys(exchange).length !== 0
        }).length === 0;
    }

    /**
     * converts the currency from btc to usd value
     * @param currency
     * @returns {string}
     */
    btcToUsd(currency) {
        const {btcBlock} = this.state;
        let dollarValOf1Btc = 1 / btcBlock.price;
        return (currency / dollarValOf1Btc.toFixed(8)).toFixed(2);
    }



    /**
     * Transforms the currency into a string displaying ether the dollar or btc sign
     * @param currency
     */
    translateCurrency(currency) {
        const {isShowingBTCCurrency} = this.state;
        if (!isShowingBTCCurrency) {
            return `$ ${this.btcToUsd(currency)}`;
        }
        return `${currency} BTC`;
    }

    // isCoinListEmpty

    render() {
        const {isBinanceApiActive, isBittrexApiActive,loading,} = this.state;
        const {btcBlock, isShowingBTCCurrency, binanceSymbolAndPrice, bittrexSymbolAndPrice, user} = this.state;
        const {calculatedBtcValue}=this.state;
        return (
            <div id="vault">
                {
                    loading? <div id="loadingScreen">
                        <CircleLoader
                            color={'#4ebc0a'}
                            loading={loading}
                            size={100}
                        />
                    </div>: null
                }
                <div id="header">
                    <div id="btcInfo">
                        <h1>Binance Btc Info</h1>
                        <main>
                            <p>Symbol {btcBlock.symbol}</p>
                            <p>Last Price $ {parseInt(btcBlock.price, 10).toFixed(3)}</p>
                        </main>
                        <CurrencySwitch
                            toggleShowing={this.toggleShowing}
                            isShowingUsCurrency={isShowingBTCCurrency}/>
                    </div>
                    <div id="title">
                        <h1 className="App-title">Coin tracker</h1>
                        <div className="btccalc">
                            <FormControl type="number" placeholder="Btc Value" onChange={(e)=> {
                                e.preventDefault();
                                this.setState({
                                    calculatedBtcValue : e.target.value
                                })
                            }}/>
                            {this.btcToUsd(calculatedBtcValue)}
                        </div>
                    </div>
                </div>
                <div id="menu">
                    {
                        // (isBinanceApiActive && isBittrexApiActive) ?
                            <CoinMenu
                                btcToUsd={this.btcToUsd}
                                updateUserCoins={this.updateUserCoins}
                                bundleImportedData={this.bundleImportedData}
                                binanceSymbolAndPrice={binanceSymbolAndPrice}
                                bittrexSymbolAndPrice={bittrexSymbolAndPrice}/>
                        // : null
                    }
                </div>
                <div id="content">
                    <div id="coinListContainer">
                        <h3 className="listTitle">User Owned Coins</h3>
                        <Form inline>
                            <FormGroup>
                                    <FormControl
                                        type="text"
                                        value={this.state.value}
                                        placeholder="Coin Search"
                                        onChange={this.handleChange}
                                        disabled={!this.isUserCoinEmpty()}
                                    />
                                    <Button disabled={!this.isUserCoinEmpty()}>Open Filter</Button>
                            </FormGroup>
                        </Form>

                        <CoinList exchanges={user.exchanges} {...this.props} toggleLoading={this.toggleLoading}/>
                        {/*TODO ticker filter bar for searching for coins */}
                        {/*TODO collapse menu for filtering */}
                    </div>
                    <div id="mainContent">
                        <Switch>
                            <Route exact path="/vault" component={Home} />
                            <Route  path="/vault/:coin" render={ props =>{
                                // checks if there is any info loaded in state
                                 return props.location.state? <CoinInfo {...props} toggleLoading={this.toggleLoading} translateCurrency={this.translateCurrency}/> :
                                    <Redirect to={{
                                        pathname: '/'
                                    }}/>
                            }} />
                        </Switch>
                    </div>
                </div>
            </div>
        );
    }
}

export default Vault;