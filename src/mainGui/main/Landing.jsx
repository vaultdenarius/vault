import React, {Component} from 'react';
import {Jumbotron, Nav, NavItem, Tab} from 'react-bootstrap'
import './styles/App.css';
import Login from './components/auth/login'
import SignUp from './components/auth/signup'
import axios from "axios/index"

/**
 * TODO Work on login in logic
 * Tie in the connection to the back end db into the login process
 * User password is whats used to unlock and use the db stored on the user own file system
 */
class Landing extends Component {
    constructor(props) {
        super(props)
        this.state = {
            changePanel: false,
            user: null
        }
        this.bindings()
    }

    bindings(){
        this.updateUser = this.updateUser.bind(this)
        this.logout = this.logout.bind(this)
    }

    updateUser(user){
        this.setState({
            user
        })
    }

    logout(){
        const {user} = this.state
        axios.post('/vault/logout',{
            ...user.user
        }).then((res)=>{
            console.log(res)
        }).catch(err => console.log(err))
    }

    render() {
        const {user} = this.state
        return (
            <div id='landing'>
                <Jumbotron>
                    <h1>Welcome to the Vault</h1>
                    <aside>
                        Privately track and gauge your coin holdings
                    </aside>
                </Jumbotron>
                <Tab.Container id="left-tabs-example" defaultActiveKey="first">
                    <div>
                        <Nav bsStyle="pills" className="landingNavs">
                            <NavItem eventKey="first">Login</NavItem>
                            <NavItem eventKey="second">Sign Up</NavItem>
                            <NavItem disabled={user === null} eventKey="" onClick={this.logout}>LogOut</NavItem>
                        </Nav>
                        <Tab.Content animation>
                            <Tab.Pane eventKey="first">
                                <Login updateUser={this.updateUser} {...this.props}/>
                            </Tab.Pane>
                            <Tab.Pane eventKey="second">
                                <SignUp updateUser={this.updateUser} {...this.props}/>
                            </Tab.Pane>
                        </Tab.Content>
                    </div>
                </Tab.Container>
            </div>
        )
    }
}

export default Landing