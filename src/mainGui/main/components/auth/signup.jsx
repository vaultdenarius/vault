import React, {Component} from 'react'
import {Button, Col, ControlLabel, Form, FormControl, FormGroup} from 'react-bootstrap'
import axios from 'axios'

class SignUp extends Component {
    constructor(props) {
        super(props)

        this.state = {
            username: '',
            email: '',
            password: ''
        }
        this.binding()
    }

    binding() {
        this.signUp = this.signUp.bind(this)
    }

    signUp() {
        const {updateUser} = this.props
        axios.post('/vault/createUser', {
            ...this.state
        }).then((res) => {
            this.setState({
                username: '',
                email: '',
                password: ''
            })
            console.log(res)
            updateUser({user: res.data})

        }).catch((err) => {
            console.log(err)
        })
    }

    render() {
        const {username,email,password} = this.state
        return (
            <Form horizontal>
                <FormGroup controlId="UserName">
                    <Col componentClass={ControlLabel} sm={2}>
                        UserName
                    </Col>
                    <Col sm={10}>
                        <FormControl type="text" placeholder="User Name" value={username}
                                     onChange={(e) => this.setState({
                                         username: e.target.value
                                     })}
                        />
                    </Col>
                </FormGroup>

                <FormGroup controlId="formHorizontalEmail">
                    <Col componentClass={ControlLabel} sm={2}>
                        Email
                    </Col>
                    <Col sm={10}>
                        <FormControl type="email" placeholder="Email" value={email}
                                     onChange={(e) => this.setState({
                                         email: e.target.value
                                     })}
                        />
                    </Col>
                </FormGroup>

                <FormGroup controlId="formHorizontalPassword">
                    <Col componentClass={ControlLabel} sm={2}>
                        Password
                    </Col>
                    <Col sm={10}>
                        <FormControl type="password" placeholder="Password" value={password}
                                     onChange={(e) => this.setState({
                                         password: e.target.value
                                     })}
                        />
                    </Col>
                </FormGroup>

                <FormGroup>
                    <Col smOffset={2} sm={10}>
                        <Button onClick={this.signUp}>Sign Up</Button>
                    </Col>
                </FormGroup>
            </Form>
        )
    }
}

export default SignUp