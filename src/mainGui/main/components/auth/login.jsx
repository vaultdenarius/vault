import React, {Component} from 'react'
import {Form,FormGroup,Col,FormControl,Button,ControlLabel} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import axios from "axios/index"


class Login extends Component{
    constructor(props){
        super(props)

        this.state = {
            email: '',
            password: ''
        }
        console.log(props)

        this.bindings()
    }


    bindings(){
        this.login = this.login.bind(this)
    }

    login(){
        const {updateUser} = this.props
        const {history} = this.props
        axios.post('/vault/login',{
            ...this.state
        }).then((res)=>{
            this.setState({
                email: '',
                password: ''
            })

            console.log(res)
            updateUser({user: res.data})

            history.push('/vault',{
                user: res.data
            })
        }).catch((err)=>{
            console.log(err)
        })
    }

    render(){
        const {email,password} = this.state
        return(
            <Form horizontal>
                <FormGroup controlId="formHorizontalEmail">
                    <Col componentClass={ControlLabel} sm={2}>
                        Email
                    </Col>
                    <Col sm={10}>
                        <FormControl type="email" placeholder="Email" value={email} onChange={(e)=>{
                            this.setState({
                                email: e.target.value
                            })
                        }}/>
                    </Col>
                </FormGroup>

                <FormGroup controlId="formHorizontalPassword">
                    <Col componentClass={ControlLabel} sm={2}>
                        Password
                    </Col>
                    <Col sm={10}>
                        <FormControl type="password" placeholder="Password" value={password} onChange={(e)=>{
                            this.setState({
                                password: e.target.value
                            })
                        }} />
                    </Col>
                </FormGroup>

                <FormGroup>
                    <Col smOffset={2} sm={10}>
                        <Button onClick={this.login}>Sign in</Button>
                    </Col>
                </FormGroup>
                <Link to={{
                    pathname: "/vault"
                }}>
                    Link to Vault
                </Link>
            </Form>
        )
    }
}

export default Login