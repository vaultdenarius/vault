var electronWorkers = require('electron-workers')({
    connectionMode: 'ipc',
    pathToScript: '/Users/xavierthomas/webProjects/tests/relectron/src/electron-starter.js',
    timeout: 5000,
    numberOfWorkers: 5
});

electronWorkers.start(function(startErr) {
    if (startErr) {
        return console.error(startErr);
    }

    // `electronWorkers` will send your data in a POST request to your electron script
    electronWorkers.execute({ someData: 'someData' }, function(err, data) {
        if (err) {
            return console.error(err);
        }

        console.log(JSON.stringify(data)); // { value: 'someData' }
        electronWorkers.kill(); // kill all workers explicitly
    });
});