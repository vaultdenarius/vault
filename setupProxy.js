const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function(app){
    app.use(
        '/api/v1.1',
        createProxyMiddleware({
            target: "https://bittrex.com",
            ws: true,
            changeOrigin: true,
        })
    );
    app.use(
        '/api',
        createProxyMiddleware({
            target: "https://api.binance.com",
            ws: true,
            changeOrigin: true,
        })
    );
    app.use(
        '/vault',
        createProxyMiddleware({
            target: "http://localhost:5000/",
            ws: true,
            changeOrigin: true,
        })
    );
}